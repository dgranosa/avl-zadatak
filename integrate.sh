#!/bin/bash

if [ -f .env ]; then
 	export $(cat .env | sed 's/#.*//g' | xargs)
else
	DOCKER_USERNAME=$1
fi

BRANCH=$(git branch --show-current)
if [[ "$BRANCH" == "master" || "$BRANCH" == "main" || "$BRANCH" == "" ]]; then
	TAG="latest"
else
	TAG=$BRANCH
fi

echo "Building service1 with tag $TAG"
docker build -t $DOCKER_USERNAME/service1:$TAG ./service1/

echo "Building service2 with tag $TAG"
docker build -t $DOCKER_USERNAME/service2:$TAG ./service2/

echo "Running Unit test"
# docker run -p 8000:8000 service1:$TAG ./unit-test.sh
# docker run -p 8080:8080 service2:$TAG ./unit-test.sh
echo "Running Integration test"
# ./integration-test.sh

echo "Login to docker"
if [ -v $DOCKER_PASSWORD ]; then
	docker login -u $DOCKER_USERNAME
else
	docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD
fi

echo "Pushing services"
docker push $DOCKER_USERNAME/service1:$TAG
docker push $DOCKER_USERNAME/service2:$TAG
