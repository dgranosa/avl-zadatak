#!/bin/bash

SERVICE1_VERSION="latest"
SERVICE2_VERSION="latest"

############# Parse arguemnts ##################
POSITIONAL=()
while [[ $# -gt 0 ]]; do
  key="$1"

  case $key in
    -vs1|--version_service1)
      SERVICE1_VERSION="$2"
      shift
      shift
      ;;
    -vs2|--version_service2)
      SERVICE2_VERSION="$2"
      shift
      shift
      ;;
    *)
      POSITIONAL+=("$1")
      shift
      ;;
  esac
done

for i in "${POSITIONAL[@]}"; do
  case $i in
    -vs1=*|--version_service1=*)
      SERVICE1_VERSION="${i#*=}"
      shift
      ;;
    -vs2=*|--version_service2=*)
      SERVICE2_VERSION="${i#*=}"
      shift
      ;;
    *)
      ;;
  esac
done
############################################

# Stop processes if active
helm uninstall service1
helm uninstall service2

# Starting services
helm install service1 service1/ --set image.tag=$SERVICE1_VERSION
helm install service2 service2/ --set image.tag=$SERVICE2_VERSION

# Waiting for services to start
kubectl rollout status -w deployment/service1
kubectl rollout status -w deployment/service2

# Port-forwarding service2 to local port
echo "Try curl localhost:8080 -d 'http://api.github.com'"
kubectl port-forward service/service2 8080:8080
