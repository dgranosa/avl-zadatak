import requests # Dodati library u requirements.txt
import sys

SERVICE1_URL = "http://service1:8080" # Namjestiti tocnu ip adresu

url = sys.stdin.readline().split("\n")[0] # Bilo je potrebno maknuti \n
message = requests.get(url).text
data = ["md5", message]

print("Hash of " + url + " is:\n" + requests.post(SERVICE1_URL, data="\n".join(data)).text)
